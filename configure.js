const { spawn } = require('child_process');
const flat = require('flat');

const options = flat({
  // Redoc theme interface: https://github.com/Redocly/redoc/blob/master/src/theme.ts
  theme: {
    spacing: {
      sectionVertical: 20,
    },
  },
  menuToggle: true,
});

function normalizeOptions() {
  return Object.keys(options).reduce((acc, optionKey) => {
    acc.push(`--options.${optionKey}=${options[optionKey]}`);

    return acc;
  }, []);
}

const args = {
  serve: ['redoc-cli', 'serve', 'content/swagger.yaml', '--watch', ...normalizeOptions()],
  bundle: ['redoc-cli', 'bundle', 'content/swagger.yaml', '-o', 'build/index.html', ...normalizeOptions()],
};

const redocCli = spawn('npx', args[process.argv[2]]);

redocCli.stdout.on('data', (data) => console.log(data.toString()));
redocCli.stderr.on('data', (data) => console.log(data.toString()));
