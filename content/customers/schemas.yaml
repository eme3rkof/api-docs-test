Customer:
  type: "object"
  description: |
    The customer object represents a company that you do business with. Retrieve a customer to get a summary of their total credit line and available credit balance.
  properties:
    id:
      type: "string"
      example: "PMMlaE5wbg0"
    created_at:
      type: "string"
      format: "date-time"
      example: "2020-01-01T00:00:00.750Z"
      description: "Date the customer was created."
    updated_at:
      type: "string"
      format: "date-time"
      example: "2020-01-01T00:00:00.750Z"
      description: "Date the customer was last updated."
    source:
      type: "string"
      enum:
        - QUICKBOOKS
        - MERCHANT_USER
        - ADMIN_USER
        - APPLICATION
        - CUSTOMER_USER
      example: "MERCHANT_USER"
    business_address:
      type: "string"
      example: "111 Main Street"
      description: "Street address of the business' primary location."
    business_city:
      type: "string"
      example: "San Francisco"
      description: "City of the business' primary location."
    business_state:
      type: "string"
      example: "CA"
      description: "State or province of the business' primary location."
    business_zip:
      type: "string"
      example: "94104"
      description: "US zip code of the business' primary location."
    business_country:
      type: "string"
      example: "US"
      description: "Country of the business' primary location according to the **ISO 3166-1 alpha 2** standard."
    business_age_range:
      type: "string"
      example: "5-10"
      description: "String indicating age of the business in years."
      enum:
        - "0-2"
        - "2-5"
        - "5-10"
        - "10+"
    business_ap_email:
      type: "string"
      example: "ap@example.com"
      description: "Email address of the business' accounts payable person or department."
    business_ap_phone:
      type: "string"
      example: "(202) 456-1414"
      description: "Phone number of the business' accounts payable person or department."
    business_name:
      type: "string"
      example: "Example, Inc."
      description: "Full legal name of the business being applied for."
    business_trade_name:
      type: "string"
      example: "Example Trading Company"
      description: "Trade name of the business, if different than `business_name.`"
    business_phone:
      type: "string"
      example: "(202) 456-1414"
      description: "Phone number of the business' primary location."
    business_type:
      type: "string"
      example: "corporation"
      description: "String indicating the business' type of legal entity."
      enum:
        - "sole_prop_or_partnership"
        - "llc"
        - "corporation"
        - "nonprofit"
        - "government"
    email:
      type: "string"
      format: "email"
      example: "user@example.com"
      description: "Email of the customer applying for terms."
    personal_name_first:
      type: "string"
      example: "James"
      description: "First name of the person applying on behalf of the business."
    personal_name_last:
      type: "string"
      example: "Bond"
      description: "Last name of the person applying on behalf of the business."
    personal_phone:
      type: "string"
      example: "(202) 456-1414"
      description: "Personal phone number of the customer representative applying for terms."
    amount_approved:
      type: "integer"
      format: "int64"
      example: 10000
      description: "Total amount of the credit approved."
    amount_authorized:
      type: "integer"
      format: "int64"
      example: 10000
      description: "Amount of the credit line reserved for authorized charges."
    amount_available:
      type: "integer"
      format: "int64"
      example: 10000
      description: "Current amount of the credit line available for purchases."
    amount_balance:
      type: "integer"
      format: "int64"
      example: 2000
      description: "Current balance on the customer's credit line."
    amount_unapplied_payments:
      type: "integer"
      format: "int64"
      example: 1000
      description: "Current amount of a customer's unapplied payments."
    default_terms:
      type: "string"
      nullable: true
      description: "Set default terms that will apply to this customer's invoices. Can be overridden when requesting an advance."
      enum:
        - "net10th"
        - "net15"
        - "net30"
        - "net45"
        - "net60"
        - "net90"
        - "net180"
        - null
    advance_rate:
      type: "number"
      format: "double"
      nullable: true
      example: 0.75
      minimum: 0
      maximum: 1
      description: "The advance rate that will be used to determine the amount advanced for this customer's invoices."

CustomerList:
  type: "object"
  properties:
    count:
      type: "integer"
      example: 1
    limit:
      type: "integer"
      example: 25
    page:
      type: "integer"
      example: 1
    results:
      items:
        $ref: "#/Customer"

CustomerPostRequest:
  type: "object"
  required:
    - "business_address"
    - "business_city"
    - "business_state"
    - "business_zip"
    - "business_country"
    - "business_name"
    - "email"
    - "business_ap_email"
  properties:
    business_address:
      type: "string"
      example: "111 Main Street"
      description: "Street address of the business' primary location."
    business_city:
      type: "string"
      example: "San Francisco"
      description: "City of the business' primary location."
    business_state:
      type: "string"
      example: "CA"
      description: "State or province of the business' primary location."
    business_zip:
      type: "string"
      example: "94104"
      description: "US zip code of the business' primary location."
    business_country:
      type: "string"
      format: "ISO 3166-1 alpha 2"
      example: "US"
      description: "2-letter country code of the business' primary location, according to the **ISO 3166-1 alpha 2** standard."
    business_ap_email:
      type: "string"
      format: "email"
      example: "ap@example.com"
      description: "Email address of the business' accounts payable person or department."
    business_ap_phone:
      type: "string"
      example: "(202) 456-1414"
      description: "Phone number of the business' accounts payable person or department."
    business_name:
      type: "string"
      example: "Example, Inc."
      description: "Full legal name of the business being applied for."
    email:
      type: "string"
      format: "email"
      example: "user@example.com"
      description: "Email of the customer applying for terms."
    default_terms:
      type: "string"
      nullable: true
      description: "Default terms invoices will be advanced with."
      enum:
        - "net10th"
        - "net15"
        - "net30"
        - "net45"
        - "net60"
        - "net90"
        - "net180"
        - null

CustomerPutRequest:
  type: "object"
  properties:
    business_address:
      type: "string"
      example: "111 Main Street"
      description: "Street address of the business' primary location."
    business_city:
      type: "string"
      example: "San Francisco"
      description: "City of the business' primary location."
    business_state:
      type: "string"
      example: "CA"
      description: "State or province of the business' primary location."
    business_zip:
      type: "string"
      example: "94104"
      description: "US zip code of the business' primary location."
    business_country:
      type: "string"
      example: "US"
      description: "Country of the business' primary location."
    business_ap_email:
      type: "string"
      example: "ap@example.com"
      description: "Email address of the business' accounts payable person or department."
    business_ap_phone:
      type: "string"
      example: "(202) 456-1414"
      description: "Phone number of the business' accounts payable person or department."
    business_name:
      type: "string"
      example: "Example, Inc."
      description: "Full legal name of the business being applied for."
    email:
      type: "string"
      format: "email"
      example: "user@example.com"
      description: "Email of the customer applying for terms."
    default_terms:
      type: "string"
      nullable: true
      description: "Set default terms that will apply to this customer's invoices. Can be overridden when requesting an advance."
      enum:
        - "net10th"
        - "net15"
        - "net30"
        - "net45"
        - "net60"
        - "net90"
        - "net180"
        - null
